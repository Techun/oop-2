public class Airline {
    private String destination;
    private int flightNumber;
    private String aircraftType;
    private String departureTime;
    private String daysOfWeek;

    // constructor
    public Airline(String destination, int flightNumber, String aircraftType, String departureTime, String daysOfWeek) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.aircraftType = aircraftType;
        this.departureTime = departureTime;
        this.daysOfWeek = daysOfWeek;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public String toString() {
        return "Airline [Des=" + destination + ", Number=" + flightNumber + ", Type=" + aircraftType
                + ", Time=" + departureTime + ", Days=" + daysOfWeek + "]";
    }
}

class List {
    private final Airline[] airlines;

    public List(Airline[] airlines) {
        this.airlines = airlines;
    }

    public void findDestination(String destination) {
        for (Airline a : airlines) {
            if (destination.contains(a.getDestination())) {
                System.out.println(a);
            }
        }
    }

    public void findDay(String dayOfWeek) {
        for (Airline a : airlines) {
            if (a.getDaysOfWeek().contains(dayOfWeek)) {
                System.out.println(a);
            }
        }
    }

    public void findDayAndTime(String dayOfWeek, String departureTime) {
        for (Airline a : airlines) {
            if (a.getDaysOfWeek().contains(dayOfWeek) && a.getDepartureTime().compareTo(departureTime) > 0) {
                System.out.println(a);
            }
        }
    }
}

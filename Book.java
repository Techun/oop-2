import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Book {
    private int id;
    private String title;
    private String[] authors;
    private String publisher;
    private int year;
    private int pages;
    private double price;
    private String bindingType;

    public Book(int id, String title, String[] authors, String publisher, int year, int pages, double price, String bindingType) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.year = year;
        this.pages = pages;
        this.price = price;
        this.bindingType = bindingType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBindingType() {
        return bindingType;
    }

    public void setBindingType(String bindingType) {
        this.bindingType = bindingType;
    }

    public String toString() {
        return "ID: " + id + ", Title: " + title + ", Authors: " + Arrays.toString(authors) + ", Publisher: " + publisher + ", Year: " + year + ", Pages: " + pages + ", Price: " + price + ", Binding Type: " + bindingType;
    }
}

class Library {
    private final Book[] books;

    public Library(Book[] books) {
        this.books = books;
    }

    public Book[] getBooksByAuthor(String author) {
        List<Book> booksByAuthor = new ArrayList<>();
        for (Book book : books) {
            for (String bookAuthor : book.getAuthors()) {
                if (bookAuthor.equals(author)) {
                    booksByAuthor.add(book);
                    break;
                }
            }
        }
        return booksByAuthor.toArray(new Book[0]);
    }

    public Book[] getBooksByPublisher(String publisher) {
        List<Book> booksByPublisher = new ArrayList<>();
        for (Book book : books) {
            if (book.getPublisher().equals(publisher)) {
                booksByPublisher.add(book);
            }
        }
        return booksByPublisher.toArray(new Book[0]);
    }

    public Book[] getBooksAfterYear(int year) {
        List<Book> booksAfterYear = new ArrayList<>();
        for (Book book : books) {
            if (book.getYear() > year) {
                booksAfterYear.add(book);
            }
        }
        return booksAfterYear.toArray(new Book[0]);
    }

    public void printBooks(Book[] books) {
        for (Book book : books) {
            System.out.println(book.toString());
        }
    }
}

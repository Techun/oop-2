public class CreateArray {
    private final String name;
    private final int groupNumber;
    private final int[] grades;

    public CreateArray(String name, int groupNumber, int[] grades) {
        this.name = name;
        this.groupNumber = groupNumber;
        this.grades = grades;
    }

    public String getName() {
        return name;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public int[] getGrades() {
        return grades;
    }

    public static void printStudents(CreateArray[] students) {
        for (CreateArray student : students) {
            for (int j = 0; j < student.getGrades().length; j++) {
                if (student.getGrades()[j] < 9) {
                    break;
                }
                if (j == student.getGrades().length - 1) {
                    System.out.println("Name student: " + student.getName() + ", Group Number: " + student.getGroupNumber());
                }
            }
        }
    }
}


public class Customer {
    private String id;
    private String surname;
    private String firstName;
    private String patronymic;
    private String address;
    private String creditCardNumber;
    private String bankAccountNumber;

    public Customer(String id, String surname, String firstName, String patronymic, String address, String creditCardNumber, String bankAccountNumber) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.creditCardNumber = creditCardNumber;
        this.bankAccountNumber = bankAccountNumber;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getId() {
        return this.id;
    }

    public String getSurname() {
        return this.surname;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getPatronymic() {
        return this.patronymic;
    }

    public String getAddress() {
        return this.address;
    }

    public String getCreditCardNumber() {
        return this.creditCardNumber;
    }

    public String getBankAccountNumber() {
        return this.bankAccountNumber;
    }

    public String toString() {
        return this.id + this.surname + this.firstName + this.patronymic + this.address + this.creditCardNumber + this.bankAccountNumber;
    }

    public void displayCustomer() {
        System.out.println(this.surname + " " + this.firstName + " " + this.patronymic);
    }
}



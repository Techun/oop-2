public class CustomerList{
    private final Customer[] customers;

    public CustomerList(Customer[] customers) {
        this.customers = customers;
    }

    public void displayAlphabetically() {
        for(int i = 1; i < this.customers.length; ++i) {
            Customer temp = this.customers[i];

            for(int j = i - 1; j >= 0; --j) {
                if (temp.getSurname().compareTo(this.customers[j].getSurname()) > 0) {
                    this.customers[j + 1] = temp;
                    break;
                }

                if (temp.getSurname().compareTo(this.customers[j].getSurname()) == 0 && temp.getFirstName().compareTo(this.customers[j].getFirstName()) > 0) {
                    this.customers[j + 1] = temp;
                    break;
                }

                if (temp.getSurname().compareTo(this.customers[j].getSurname()) == 0 && temp.getFirstName().compareTo(this.customers[j].getFirstName()) == 0 && temp.getPatronymic().compareTo(this.customers[j].getPatronymic()) >= 0) {
                    this.customers[j + 1] = temp;
                    break;
                }

                this.customers[j + 1] = this.customers[j];
                if (j == 0) {
                    this.customers[0] = temp;
                }
            }
        }

        this.displayCustomers();
    }

    public void displaySpecifiedInterval(int interval) {
        for(int i = 0; i < this.customers.length; ++i) {
            if (this.customers[i].getCreditCardNumber().length() <= interval) {
                this.customers[i].displayCustomer();
            }
        }

    }

    public void displayCustomers() {
        Customer[] var1 = this.customers;
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            Customer temp = var1[var3];
            temp.displayCustomer();
        }

    }
}

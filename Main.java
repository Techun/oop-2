public class Main {
    public static void main(String[] args) {
        //TASK1
        System.out.println("\n\nTASK1");
        CreateArray[] students = new CreateArray[10];
        students[0] = new CreateArray("John", 1, new int[]{9, 8, 7, 6, 9});
        students[1] = new CreateArray("Jane", 2, new int[]{10, 9, 8, 7, 6});
        students[2] = new CreateArray("Bob", 3, new int[]{7, 8, 9, 10, 9});
        students[3] = new CreateArray("Samantha", 4, new int[]{9, 9, 9, 9, 9});
        students[4] = new CreateArray("Michael", 5, new int[]{10, 10, 10, 10, 10});
        students[5] = new CreateArray("Emily", 6, new int[]{9, 8, 7, 6, 5});
        students[6] = new CreateArray("Jac", 7, new int[]{9, 9, 9, 9, 9});
        students[7] = new CreateArray("Nicholas", 8, new int[]{10, 10, 10, 10, 10});
        students[8] = new CreateArray("Amanda", 9, new int[]{8, 8, 7, 6, 5});
        students[9] = new CreateArray("Vlad", 10, new int[]{9, 9, 9, 9, 9});

        System.out.println("students with grades of only 9 or 10:");
        CreateArray.printStudents(students);


        //task2
        System.out.println("\n\nTASK2");
        Customer[] customers = new Customer[]{new Customer("FFF", "HASHKIN", "Mikita", "Hit", "slums 15", "7698302", "77888877r"), new Customer("kin", "Kulak", "Vadim", "What", "NEter 11", "0939402874", "8525230a"), new Customer("KI", "Litvin", "Ilya", "Vich", "Seno", "03120492", "08275893270o"), new Customer("SAD", "Litvin", "Ilya", "Luch", "Skaya 21", "3210423943123412", "23578327e")};
        CustomerList customerList = new CustomerList(customers);
        customerList.displaySpecifiedInterval(10);


        //task3
        System.out.println("\n\nTASK3");
        Train[] trains = new Train[]{new Train("Hogwarts", 3, "12:30"), new Train("London", 4, "13:30"), new Train("Warsaw", 10, "10:00"), new Train("Minsk", 11, "08:00"), new Train("Madrid", 1, "01:15")};
        Train.showTrainInfo(3, trains);


        //TASK4
        System.out.println("\n\nTASK4");
        Book book1 = new Book(1, "Catcher", new String[]{"John"}, "Little Company", 1951, 277, 14.99, "Hardcover");
        Book book2 = new Book(2, "Kill bird", new String[]{"Bob"}, "Go go", 1960, 281, 12.99, "Softcover");
        Book book3 = new Book(3, "1990", new String[]{"Nicholas"}, "Wanted", 1999, 328, 17.99, "Hardcover");
        Book book4 = new Book(4, "Great", new String[]{"Scott"}, "Not cool", 1925, 180, 11.99, "Softcover");
        Book book5 = new Book(5, "Witch", new String[]{"Vlad"}, "Viking", 1939, 467, 16.99, "Hardcover");
        Book[] books = new Book[]{book1, book2, book3, book4, book5};
        Library library = new Library(books);

        System.out.println("Books by Bob: ");
        library.printBooks(library.getBooksByAuthor("Bob"));
        System.out.println("\nBooks by Little Company: ");
        library.printBooks(library.getBooksByPublisher("Little Company"));
        System.out.println("\nBooks after 1990: ");
        library.printBooks(library.getBooksAfterYear(1990));


        //TASK5
        System.out.println("\n\nTASK5");
        Airline airline1 = new Airline("New York", 123, "Boeing 777", "10:00", "Monday, Wednesday, Friday");
        Airline airline2 = new Airline("Paris", 456, "Boeing 1337", "12:00", "Tuesday, Thursday, Saturday");
        Airline airline3 = new Airline("Moscow", 789, "Boeing 228", "2:00", "Sunday, Monday, Wednesday");
        Airline[] airlines = {airline1, airline2, airline3};
        List airlineList = new List(airlines);

        System.out.println("\nFlights to New York: ");
        airlineList.findDestination("New York");

        System.out.println("\nFlights on Wednesday: ");
        airlineList.findDay("Wednesday");

        System.out.println("\nFlights on Monday after 12pm: ");
        airlineList.findDayAndTime("Monday", "12:00pm");
    }
}


public class Train {
    private final String destinationName;
    private final int trainNumber;
    private final String departureTime;

    public Train(String destinationName, int trainNumber, String departureTime) {
        this.destinationName = destinationName;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
    }

    public static void sortByTrainNumber(Train[] trains) {
        for(int i = 0; i < trains.length; ++i) {
            Train minimum = trains[i];
            int trainNumberInTheArray = i;

            for(int j = i + 1; j < trains.length; ++j) {
                if (minimum.trainNumber > trains[j].trainNumber) {
                    minimum = trains[j];
                    trainNumberInTheArray = j;
                }
            }

            Train temp = trains[i];
            trains[i] = minimum;
            trains[trainNumberInTheArray] = temp;
            System.out.println(trains[i].trainNumber);
        }

    }

    public static void sortByDestination(Train[] trains) {
        for(int i = 0; i < trains.length - 1; ++i) {
            for(int j = 0; j < trains.length - 1; ++j) {
                Train temp;
                if (trains[j].destinationName.compareTo(trains[j + 1].destinationName) > 0) {
                    temp = trains[j];
                    trains[j] = trains[j + 1];
                    trains[j + 1] = temp;
                } else if (trains[j].destinationName.compareTo(trains[j + 1].destinationName) == 0 && trains[j].departureTime.compareTo(trains[j + 1].departureTime) > 0) {
                    temp = trains[j];
                    trains[j] = trains[j + 1];
                    trains[j + 1] = temp;
                }
            }
        }

    }

    public static void showTrainInfo(int trainNumber, Train[] trains) {
        Train[] var2 = trains;
        int var3 = trains.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Train temp = var2[var4];
            if (temp.trainNumber == trainNumber) {
                System.out.println("Train number " + temp.trainNumber + " departs to " + temp.destinationName + " at - " + temp.departureTime);
                return;
            }
        }

        System.out.println("There is not such train");
    }

    public String getDestinationName() {
        return this.destinationName;
    }

    public int getTrainNumber() {
        return this.trainNumber;
    }

    public String getDepartureTime() {
        return this.departureTime;
    }
}
